﻿using System.Collections.Generic;

namespace SugarScript.CodeSource
{
  /// <summary>
  /// Interface for our CodeSource, for now it's just an enumerable of strings, but will most likely grow
  /// </summary>
  public interface ICodeSource : IEnumerable<string> { }
}
