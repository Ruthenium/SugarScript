﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SugarScript.CodeSource
{
  /// <summary>
  /// When the source of the code comes from a file, we use this CodeSource
  /// </summary>
  public class FileSource : ICodeSource
  {
    private string[] linesInFile;

    public FileSource(string path)
    {
      if (File.Exists(path))
      {
        linesInFile = File.ReadAllLines(path);
      }
      else
      {
        throw new Exception($"File {path} cannot be found!");
      }
    }

    public IEnumerator<string> GetEnumerator()
    {
      return linesInFile.Cast<string>().GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return linesInFile.GetEnumerator();
    }
  }
}
