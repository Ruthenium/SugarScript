﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace SugarScript.CodeSource
{
	/// <summary>
	/// When the source of the code is from the console, we use this CodeSource
	/// </summary>
	public class ConsoleLineSource : ICodeSource
	{
		private const string CARRET = ">>> ";

		public IEnumerator<string> GetEnumerator()
		{
			return new ConsoleLineEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return new ConsoleLineEnumerator();
		}

		internal class ConsoleLineEnumerator : IEnumerator<string>
		{
			public string Current
			{
				get
				{
					Console.Write(CARRET);
					return Console.ReadLine();
				}
			}

			object IEnumerator.Current
			{
				get
				{
					return Console.ReadLine();
				}
			}

			public bool MoveNext()
			{
				return true;
			}

			public void Reset() { }
			public void Dispose() { }
		}
	}
}
