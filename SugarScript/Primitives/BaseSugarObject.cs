﻿using SugarScript.Primitives.Functions;

namespace SugarScript.Primitives
{
	public abstract class BaseSugarObject
	{
		private bool _isFunction;
		private bool _isNull;

		public BaseSugarObject()
		{
			_isFunction = this is SugarFunction;
		}

		public bool IsFunction { get { return _isFunction; } }

		public bool IsNull { get { return _isNull; } }
	}
}
