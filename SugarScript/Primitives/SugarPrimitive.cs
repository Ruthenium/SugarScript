﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SugarScript.Primitives
{
	public class SugarPrimitive<T> : BaseSugarObject
	{
		private T _value;

		public SugarPrimitive(T value) : base() { }

		public T Value { get { return _value; } }
	}
}
