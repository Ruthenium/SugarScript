﻿using SugarScript.Scopes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SugarScript.Primitives.Functions.SystemFunctions
{
	public class CreateVariableFunction : SugarFunction
	{
		public CreateVariableFunction(IScope scope) : base(scope, new List<string>() { "name", "value" }) { }

		public override BaseSugarObject Execute(IScope executionContext, List<BaseSugarObject> paramValues)
		{
			StringPrimitive name = paramValues[0] as StringPrimitive;
			BaseSugarObject value = paramValues[1];
			if (executionContext.HasObject(name.Value))
			{
				throw new Exception($"Tried to create variable with name {name.Value} but it already exists in current context");
			}

			executionContext.AddObject(name.Value, value);

			return value;
		}
	}
}
