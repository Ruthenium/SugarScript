﻿using SugarScript.Scopes;
using System.Collections.Generic;

namespace SugarScript.Primitives.Functions
{
	public abstract class SugarFunction : BaseSugarObject
	{
		List<string> _parameters;
		IScope _functionScope;

		public SugarFunction(IScope parentScope, List<string> parameters) : base()
		{
			_parameters = parameters;
			_functionScope = new SugarScope(parentScope);
		}

		protected List<string> ParameterNames { get { return _parameters; } }

		public abstract BaseSugarObject Execute(IScope executionContext, List<BaseSugarObject> paramValues);
	}
}
