﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SugarScript.Primitives
{
	public class StringPrimitive : SugarPrimitive<string>
	{
		public StringPrimitive(string value) : base(value) { }
	}
}
