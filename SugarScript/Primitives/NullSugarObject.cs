﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SugarScript.Primitives
{
	public class NullSugarObject : BaseSugarObject
	{
		private static NullSugarObject _instance = new NullSugarObject();

		public static NullSugarObject Instance { get { return _instance; } }
	}
}
