﻿using SugarScript.Primitives;
using System.Collections.Generic;

namespace SugarScript.Scopes
{
	public interface IScope
	{
		void AddObject(string name, BaseSugarObject toAdd);
		bool HasObject(string name);
		BaseSugarObject GetObject(string name);
		List<string> AllFunctionNames { get; }
	}
}
