﻿using SugarScript.Primitives;
using System;
using System.Linq;
using System.Collections.Generic;

namespace SugarScript.Scopes
{
	public class SugarScope : IScope
	{
		private Dictionary<string, BaseSugarObject> _scopeObjectDic;
		private IScope _parentScope;

		public SugarScope() : this(new EmptyScope()) { }

		public SugarScope(IScope parentScope)
		{
			_parentScope = parentScope;
			_scopeObjectDic = new Dictionary<string, BaseSugarObject>();
		}

		public void AddObject(string name, BaseSugarObject toAdd)
		{
			if (_scopeObjectDic.ContainsKey(name))
			{
				throw new Exception($"There is already a variable created with the name {name}!");
			}
			else
			{
				_scopeObjectDic.Add(name, toAdd);
			}
		}

		public bool HasObject(string name)
		{
			return _scopeObjectDic.ContainsKey(name) || _parentScope.HasObject(name);
		}


		public List<string> AllFunctionNames
		{
			get
			{
				return _scopeObjectDic.Where(x => x.Value.IsFunction).Select(x => x.Key).Concat(_parentScope.AllFunctionNames).ToList();
			}
		}


		public BaseSugarObject GetObject(string name)
		{
			if (!HasObject(name)) throw new Exception($"Variable of name {name} could not be found in current scope!");

			bool isInCurrentScope = _scopeObjectDic.ContainsKey(name);
			return isInCurrentScope ? _scopeObjectDic[name] : _parentScope.GetObject(name);
		}
	}
}
