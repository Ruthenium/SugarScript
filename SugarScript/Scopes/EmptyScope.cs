﻿using SugarScript.Primitives;
using System;
using System.Collections.Generic;

namespace SugarScript.Scopes
{
	public class EmptyScope : IScope
	{
		public List<string> AllFunctionNames
		{
			get
			{
				return new List<string>();
			}
		}

		public void AddObject(string name, BaseSugarObject toAdd)
		{
			throw new Exception($"Tried to add object of name {name} in an empty scope!");
		}

		public BaseSugarObject GetObject(string name)
		{
			throw new Exception($"Tried to get object of name {name} in an empty scope!");
		}

		public bool HasObject(string name)
		{
			return false;
		}
	}
}
