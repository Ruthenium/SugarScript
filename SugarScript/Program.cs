﻿using SugarScript.CodeSource;
using SugarScript.Primitives;
using SugarScript.Primitives.Functions;
using SugarScript.Primitives.Functions.SystemFunctions;
using SugarScript.Scopes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SugarScript
{
	class Program
	{
		const char FUNCTION_CALL_TOKEN = '(';
		static void Main(string[] args)
		{
			ICodeSource source = GetCodeSource(args);
			SugarScope globalScope = new SugarScope();
			globalScope.AddObject("CreateVariable", new CreateVariableFunction(globalScope));
			foreach (var line in source)
			{
				if (line.Equals("exit", StringComparison.InvariantCultureIgnoreCase)) break;

				ExecuteLine(globalScope, line);
			}
			Console.ReadLine();
		}

		static BaseSugarObject ExecuteLine(IScope executionContext, string line)
		{
			// Tries and find an object that supports the current line

			int indexOfFirstCallToken = line.IndexOf(FUNCTION_CALL_TOKEN);
			if (indexOfFirstCallToken != -1)
			{
				string potentialFunctionName = line.Substring(0, indexOfFirstCallToken);
				bool functionExists = executionContext.AllFunctionNames.Any(x => x.Equals(potentialFunctionName));
				if (functionExists)
				{
					SugarFunction func = executionContext.GetObject(potentialFunctionName) as SugarFunction;
					// TODO(gboudreau) : parse parameters and pass them to the function
				}
			}
			return NullSugarObject.Instance;
		}

		/// <summary>
		/// Returns the source of the code depending on the arguments
		/// </summary>
		private static ICodeSource GetCodeSource(string[] args)
		{
			if (args.Length > 0)
			{
				return new FileSource(args[0]);
			}
			else
			{
				return new ConsoleLineSource();
			}
		}
	}
}
